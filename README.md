# CKI Project Container Images

This repo contains the build scripts for the container images used by the CKI
Project. Each container image is built and hosted in GitLab CI using
[buildah](https://github.com/containers/buildah).

## Project Details

### Maintainers

[Iñaki Malerba](https://gitlab.com/inakimalerba)
[Michael Hofmann](https://gitlab.com/mh21)

### Design principles

* Only container image build files (sometimes called *Dockerfiles*) belong in
  the `builds` directory. Any additional content, such as certificates,
  repositories, or configuration files must be in the `files` directory.
* Tags must be in a special date format: `YYYYMMDD.<revision>`. For example,
  the second revision from July 4, 2019 would be tagged `20190704.2`.
* Nearly all containers are based on the latest Fedora release.
* Eventually, this repository should only contain:
  * a `base` container image for derived images
  * a `python` container image for all CI/CD work and the pipeline Python stages
  * various `builder` container images for the pipeline build stages

### Developer guidelines

* All container images are built and pushed with the `latest` tag by default.
  This means that you can push a change and those builds will replace the last
  `latest` tag.
* Almost all CKI projects pull from a specific date tag, such as `20190917.1`,
  so there is very little risk of damaging an active service by applying a new
  tag and pushing container images to that tag. Each CKI project would need to
  be updated to ask for the new tagged container image.
* Keep container images small by cleaning up after yourself within the
  container. Watch out for applications that leave behind lots of cached data.

## Container image building blocks

To build a container image out of an application, add a publish job to your
`.gitlab-ci.yml` like this:

```
include: https://gitlab.com/cki-project/containers/-/raw/master/includes/gitlab-publish.yml

stages:
  - 📦

publish:
  extends: .publish
```

Take a look at the [publish job template](includes/gitlab-publish.yml) to see
what variables can be overridden.  The `base_image_tag` and `buildah_image_tag`
variables allow to run tests builds of your application with newer images of
the `base` and `buildah` container images.

Additionally, create the build specification in `builds/application-name.in`.
Build specifications ending in `.in` will be preprocessed with the C
preprocessor `cpp` by `buildah`. This allows to include the following building
blocks with `#include`:

* `setup-base`: `FROM` command for a generic Python application based on the
  `base` container image with support for customizing image name and tag
* `python-requirements`: commands for a generic Python application that
  contains `requirements.txt` and `run.py` files
* `cleanup`: remove caches from `dnf` and `pip`, should always be at the end of
  your build specification

As an example, the build specification for a Python application in
`application-name.in` could look like

```
#include "setup-base"
#include "python-requirements"
#include "cleanup"
```

As the preprocessor will consider anything starting with a `#` as a
preprocessor command, comments need to be put between C-style delimiters like
`/* comment */`.

## Current container images

This repository currently provides the following container images:

### base

This container image is the preferred base image of derived container images
and only includes a minimal Python 3 and pip environment.

### python

This container image is the preferred image for all CI/CD pipelines in GitLab.
Next to Python 3, it includes everything that is needed to run tests for CKI projects.

### builder-fedora

This container image can compile upstream Linux kernels and it also includes
extra packages for python, including `crudini` for managing ini files.

### builder-rawhide

Similar to `builder-fedora`, but based on a Rawhide environment.

### buildah (deprecated)

This container image can run buildah to build container images.
It is not automatically rebuilt, i.e. the GitLab build job needs to be started manually.
Eventually, this container image should be folded into the Python container image.

### createrepo (deprecated)

This container image can create RPM repositories and handle downloading kernels
from Brew or Koji.
Eventually, this container image should be folded into the Python container image.

### openshift-client (deprecated)

This container image has the utilities needed to talk to an OpenShift cluster
and run various operations there.
Eventually, this container image should be folded into the Python container image.

### test (deprecated)

This container image has the basics for talking to Beaker, including the Beaker
client, python, and kerberos.
Eventually, this container image should be folded into the Python container image.

### monit

This container image contains a monit service ready to go.
