#!/usr/bin/python3
"""Check the usage of an S3 bucket against quota and threshold."""
import argparse
import collections
import os
import sys

import boto3
import botocore
from botocore.config import Config

BucketSpec = collections.namedtuple(
    'BucketSpec', ('endpoint', 'access_key', 'secret_key', 'bucket', 'prefix'))


class StoreBucketSpec(argparse.Action):
    """Parse bucket specs."""

    def __call__(self, parser, namespace, values, option_string=None):
        """Parse the contents of a bucket spec env variable."""
        setattr(namespace, self.dest,
                BucketSpec(*os.environ[values].split('|')))


parser = argparse.ArgumentParser(
    description='Check the usage of an S3 bucket against quota and threshold.')
parser.add_argument('bucket', action=StoreBucketSpec,
                    help='name of a bucket spec environment variable')
parser.add_argument('--quota', type=int,
                    help='bucket quota in GB')
parser.add_argument('--threshold', type=int,
                    help='percentage of quota for notification via exit code')
parser.add_argument('--ignore-prefix', action='store_true',
                    help='report the usage for the whole bucket')
args = parser.parse_args()

config = Config()
if not args.bucket.access_key:
    config.signature_version = botocore.UNSIGNED
s3 = boto3.resource('s3',
                    endpoint_url=args.bucket.endpoint or None,
                    aws_access_key_id=args.bucket.access_key,
                    aws_secret_access_key=args.bucket.secret_key,
                    config=config)
bucket = s3.Bucket(args.bucket.bucket)
prefix = '' if args.ignore_prefix else args.bucket.prefix
size = 0
count = 0
for bucket_object in bucket.objects.filter(Prefix=prefix):
    size += bucket_object.size
    count += 1
size = size / 1e9

if args.quota:
    used = size / args.quota * 100
    print(f'{used:.1f}% [{size:.1f} GB, {count} objects]')
    sys.exit(1 if args.threshold and used >= args.threshold else 0)

print(f'[{size:.1f} GB, {count} objects]')
