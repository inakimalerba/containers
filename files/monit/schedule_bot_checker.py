#!/usr/bin/python3
import datetime
import os
import pathlib
import sys

from crontab import CronTab
from cki_lib import kubernetes


def get_last_terminated_pod(pods, name):
    """Return the last terminated pod for schedule {name}."""
    task_pods = [p for p in pods if p.metadata.labels['schedule_job'] == name]
    for pod in sorted(task_pods, key=lambda x: x.metadata.creation_timestamp, reverse=True):
        if pod.status.container_statuses[0].state.terminated:
            return pod


def check_pod_succeeded(k8s_helper, pod):
    """Check if the execution was successful."""
    termination_reason = pod.status.container_statuses[0].state.terminated.reason
    if termination_reason != 'Completed':
        return f'Terminated: {termination_reason}'


def check_time_since_execution(k8s_helper, pod):
    """
    Check that the last execution was before the period configured.

    Using the cron_job schedule, calculate when the next schedule should have been.
    If the expected_next_execution_in is negative, it means that the task should
    have been already executed and we missed the schedule.
    """
    task_name = pod.metadata.labels['schedule_job']
    last_execution = pod.metadata.creation_timestamp

    api = k8s_helper.api_batchv1beta1()
    cron = api.read_namespaced_cron_job(task_name, k8s_helper.namespace)

    expected_next_execution_in = CronTab(cron.spec.schedule).next(now=last_execution)

    if expected_next_execution_in < 0:
        return f'Schedule missed: {expected_next_execution_in * -1} sec ago'


def main():
    k8s_helper = kubernetes.KubernetesHelper()
    k8s_helper.setup()
    api = k8s_helper.api_corev1()

    all_pods = api.list_namespaced_pod(k8s_helper.namespace).items
    scheduler_pods = [p for p in all_pods if p.metadata.name.startswith('schedule-bot')]
    scheduler_tasks = {p.metadata.labels['schedule_job'] for p in scheduler_pods}

    checkers = [check_pod_succeeded, check_time_since_execution]
    errors = []
    for task_name in scheduler_tasks:
        pod = get_last_terminated_pod(scheduler_pods, task_name)
        for check in checkers:
            error = check(k8s_helper, pod)
            if error:
                errors.append(f'{task_name} ({error})')
    if errors:
        print(', '.join(errors))
        sys.exit(1)

main()
