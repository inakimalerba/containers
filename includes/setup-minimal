#ifdef _BASE_IMAGE
ARG BASE_IMAGE=_BASE_IMAGE
#else
ARG BASE_IMAGE=fedora
#endif
#ifdef _BASE_IMAGE_TAG
ARG BASE_IMAGE_TAG=_BASE_IMAGE_TAG
#else
ARG BASE_IMAGE_TAG=31
#endif

FROM $BASE_IMAGE:$BASE_IMAGE_TAG
LABEL maintainer="CKI Project Team <cki-project@redhat.com>"

/* Add the Red Hat CAs */
ADD files/rh-*.crt /etc/pki/ca-trust/source/anchors/
RUN update-ca-trust

/* Configure Python */
ENV REQUESTS_CA_BUNDLE="/etc/ssl/certs/ca-bundle.crt"
ENV PYTHONUNBUFFERED=1

/* Configure dnf */
RUN echo "fastestmirror=true" >> /etc/dnf/dnf.conf
RUN echo "install_weak_deps=false" >> /etc/dnf/dnf.conf
RUN echo "best=true" >> /etc/dnf/dnf.conf

/* gnome-keyring uses file capabilities which breaks buildah */
RUN echo "exclude=gnome-keyring" >> /etc/dnf/dnf.conf

/* Pin shadow-utils due to breakage on podman.
   cpio: cap_set_file failed - Inappropriate ioctl for device */
RUN dnf -y install dnf-plugin-versionlock
RUN dnf versionlock add shadow-utils

/* Upgrade all packages */
RUN dnf -y upgrade

/* Make RPM happy with arbitrary UID/GID in OpenShift */
RUN chmod g=u /etc/passwd /etc/group

/* Minimal Python 3 stack able to get pip packages from git. */
RUN dnf -y install python3 python3-pip git-core

/* Until proper monitoring arrives, allow basic memory/cpu inspection. */
RUN dnf -y install procps
