#!/bin/bash
set -euo pipefail

# Make a note of the package versions.
buildah --version

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

function say {
    echo "$@" | toilet -f mono12 -w 400 | lolcat -f || true
}

# Build the container.
say "build"
function build_image {
    CPATH=includes:/usr/local/share/cki buildah bud \
        ${base_image+--build-arg BASE_IMAGE=$base_image} \
        ${base_image_tag+--build-arg BASE_IMAGE_TAG=$base_image_tag} \
        -f "builds/${IMAGE_NAME}"* \
        -t "${IMAGE_NAME}" \
        .
}
# try to cope with networking and registry issues
for i in {1..5}; do build_image && s=0 && break || s=$? && sleep 10; done; (exit $s)

# Only continue if running in a CI/CD setup
if [ ! -v CI_COMMIT_REF_NAME ]; then
    exit
fi

# If a tag was provided, use it for the image. If we're on the master branch,
# use 'latest'.
if [ -n "${CI_COMMIT_TAG:-}" ]; then
    IMAGE_TAG=${CI_COMMIT_TAG}
elif [ "$CI_COMMIT_REF_NAME" = 'master' ]; then
    IMAGE_TAG="latest"
fi

# For containers that should not be tagged, just update the latest.
if [[ "${IMAGE_TAG:-}" != "" ]] && [[ "${LATEST_ONLY:-}" = 'true' ]]; then
    IMAGE_TAG="latest"
fi

# If we are not on a tag or on the master branch, stop here. There is no need
# to push.
if [ -z "${IMAGE_TAG:-}" ]; then
    exit 0
fi

if [ -n "${CI_REGISTRY:-}" ]; then
    # Prepare to push to GitLab's container registry.
    say "gitlab push"
    export REGISTRY_AUTH_FILE=${HOME}/auth.json
    echo "${CI_REGISTRY_PASSWORD}" | buildah login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    GITLAB_TAG=${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_TAG}
    buildah tag "${IMAGE_NAME}" "${GITLAB_TAG}"

    # Push to registry.gitlab.com.
    for i in {1..5}; do
        echo "Attempt #${i} to push to registry.gitlab.com..."
        if buildah push "${GITLAB_TAG}"; then
            PUSH_GITLAB_OK=1
            break
        fi
    done
else
    PUSH_GITLAB_OK=1
fi

if [ -n "${INTERNAL_DOCKER_AUTH_JSON:-}" ]; then
    # Prepare to push to the internal Docker registry.
    say "internal push"
    export REGISTRY_AUTH_FILE=${INTERNAL_DOCKER_AUTH_JSON}
    buildah login "${INTERNAL_DOCKER_URL}"
    INTERNAL_DOCKER_TAG="${INTERNAL_DOCKER_URL}/cki/${IMAGE_NAME}:${IMAGE_TAG}"
    buildah tag "${IMAGE_NAME}" "${INTERNAL_DOCKER_TAG}"

    # Push to internal docker registry.
    for i in {1..5}; do
        echo "Attempt #${i} to push to ${INTERNAL_DOCKER_URL}..."
        if buildah push "${INTERNAL_DOCKER_TAG}"; then
            PUSH_INTERNAL_OK=1
            break
        fi
    done
else
    PUSH_INTERNAL_OK=1
fi

if [[ -n ${QUAY_IO_PASSWORD:-} ]] && [[ -n ${QUAY_IO_USER:-} ]]; then
    # Prepare to push to quay.io's container registry.
    say "quay push"
    echo "$QUAY_IO_PASSWORD" | buildah login -u "$QUAY_IO_USER" --password-stdin quay.io
    QUAY_TAG=quay.io/cki/${IMAGE_NAME}:${IMAGE_TAG}
    buildah tag "${IMAGE_NAME}" "${QUAY_TAG}"

    # Push to quay.io.
    for i in {1..5}; do
        echo "Attempt #${i} to push to quay.io..."
        if buildah push "${QUAY_TAG}"; then
            PUSH_QUAY_OK=1
            break
        fi
    done
else
    PUSH_QUAY_OK=1
fi

# If any push failed, fail the pipeline.
if [[ -z ${PUSH_GITLAB_OK:-} ]] || [[ -z ${PUSH_INTERNAL_OK:-} ]] || [[ -z ${PUSH_QUAY_OK:-} ]]; then
    echo "The container push failed to one or more destinations."
    exit 1
fi
